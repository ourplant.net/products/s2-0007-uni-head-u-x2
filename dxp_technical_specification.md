The S2-0007 UNI-HEAD U-X2 has the following technical specifications.

<!-- ## Technical information -->

|Name                       |              Value|
|:----|----:|
| Weight                    |            5.2 kg |
| Dimensions                | 79 × 205 × 514 mm |
| Movement range in Z in mm |               150 |
| Voltage in V              |                24 |
| Max. current in A         |                 4 |
| Communication interface   |             UNICAN|


